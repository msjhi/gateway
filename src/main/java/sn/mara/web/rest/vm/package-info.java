/**
 * View Models used by Spring MVC REST controllers.
 */
package sn.mara.web.rest.vm;
